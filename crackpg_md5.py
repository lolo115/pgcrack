#!/usr/bin/python

# [postgres@pg hack]$ cat md5_sample.txt
# laurent,e545ada4064f90f143297ad0a67fdbe2
# postgres,3175bce1d3201d16594cebf9d7eb3f9d



import hashlib
import binascii
import sys

def next_n_lines(file_opened, N):
    return [x.strip() for x in islice(file_opened, N)]

#main
# Usage:
# ./crackpg_md5.py md5_sample.txt testpwd.txt
# md5_sample.txt contains rolename,md5_hash(without md5 prefix)
# testpwd.txt contains a list a password to test (password dictionary)

if __name__ == '__main__':
        print "File = ",sys.argv[1]

        with open(sys.argv[2],'r') as pf:
          pwd_tab=pf.read().splitlines()
        print "pwd_size",len(pwd_tab)

        f=open(sys.argv[1],'r')
        lines=f.read().splitlines()
        f.close()
        for l in lines:
          print '=========================================='
          sep=l.split(',')
          print "name =",sep[0]
          print "md5  =",sep[1]

          pwd_tab.extend([sep[0]])
          for i in range(len(pwd_tab)):
            #print "name =",sep[0],"  - mdp: ",pwd_tab[i]," -  hash ",hashlib.md5(sep[0]+pwd_tab[i]).hexdigest()
            if (hashlib.md5(sep[0]+pwd_tab[i]).hexdigest() == sep[1]) :
              print "Password trouve : ",pwd_tab[i]
              break

